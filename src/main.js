import Vue from 'vue'
import App from './App.vue'

// router
import router from './router'

// bootstrap
import 'bootstrap'
import 'bootstrap/dist/css/bootstrap.min.css'

// style from /src/styles
import './styles/vars.scss'
import './styles/general.scss'
import './styles/home.scss'

Vue.config.productionTip = false

new Vue({
  router,
  render: h => h(App)
}).$mount('#app')
